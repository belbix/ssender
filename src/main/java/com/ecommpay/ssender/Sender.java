package com.ecommpay.ssender;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.ecommpay.ssender.Main.MAX_TIME_WORK;
import static java.util.Objects.isNull;

public class Sender {
    public static final byte[] IDLE_ARRAY = {0, 0, 0, 0};
    private final Socket socket;
    private InputStream is = null;
    private OutputStream os = null;
    private BlockingQueue<String> income = new LinkedBlockingQueue<>();

    public Sender(Socket socket) {
        this.socket = socket;
        new Thread(this::run).start();
    }

    public void send(String msg) {
        System.out.println("Send " + msg);
        byte[] arr = hexStringToByteArray(msg);
        try {
            Instant start = Instant.now();
            while (os == null && Duration.between(start, Instant.now()).toMinutes() < MAX_TIME_WORK){}
            os.write(arr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        System.out.println("Start wait answers...");
        try {
            if (socket != null && !socket.isClosed()) {
                socket.setReceiveBufferSize(100000);
                socket.setKeepAlive(true);

                is = socket.getInputStream();
                os = socket.getOutputStream();

                long msgCount = 0;
                Instant start = Instant.now();
                while (Duration.between(start, Instant.now()).toMinutes() < MAX_TIME_WORK) {
                    try {
                        byte[] result = handleIS(is);
                        String msg = bytesToHexString(result);
                        if(msg.equals("00000000")) continue;
                        income.add(msg);
                    } catch (SocketException e) {
                        System.out.println("Socket error");
                        break;
                    } catch (Exception e) {
                        System.out.println("Error read msg:" + msgCount);
                    }

                    msgCount++;
                }
                if(msgCount == 0){
                    System.out.println("Didn't receive any messages");
                }
                System.exit(-1);
            }
        } catch (Exception e) {
            System.out.println("Socket main loop error");
        }
    }

    public static byte[] handleIS(InputStream is) throws IOException {
        byte[] readBuffer = new byte[100000];
        int length = readIS(is, readBuffer);
        if (length == -1) {
            throw new SocketException("Socket closed by signal -1");
        }

        byte[] buffer = new byte[length];
        System.arraycopy(readBuffer, 0, buffer, 0, length);

        if (length < IDLE_ARRAY.length) {
            throw new RuntimeException("Socket get garbage bytes:" + bytesToHexString(buffer));
        }

        return buffer;

    }

    private static int readIS(InputStream is, byte[] readBuffer) throws IOException {
        int bytesRead;
        try {
            bytesRead = is.read(readBuffer, 0, readBuffer.length);
        } catch (SocketException e) {
            if (e.getMessage().equals("Socket closed") || e.getMessage().equals("Connection reset")) {
                return -1;
            } else {
                throw e;
            }
        }
        return bytesRead;
    }

    public BlockingQueue<String> getIncome() {
        return income;
    }

    private static String bytesToHexString(byte[] b) {
        final char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        if ((b == null) || (b.length == 0)) {
            throw new IllegalArgumentException("argument null or zero length");
        }
        StringBuilder buff = new StringBuilder(b.length * 2);

        for (int i = 0; i < b.length; i++) {
            buff.insert(i * 2, chars[(b[i] >> 4) & 0xf]);
            buff.insert(i * 2 + 1, chars[b[i] & 0xf]);
        }

        return buff.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        if (isNullOrEmpty(s)) return new byte[0];
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static boolean isNullOrEmpty(String param) {
        return "null".equals(param) || isNull(param) || param.isEmpty();
    }
}
