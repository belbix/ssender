package com.ecommpay.ssender;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static com.ecommpay.ssender.Main.MAX_TIME_WORK;

public class MSocket implements Runnable {
    private final String ip;
    private final int port;

    private ServerSocket server;
    private InputStream is = null;
    private OutputStream os = null;

    public MSocket(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void openSocket(String ip, int port) throws IOException {
        server = new ServerSocket();
        InetSocketAddress adr = new InetSocketAddress(ip, port);
        server.bind(adr);
        if (server.isBound()) {
            System.out.println("Waiting Connection...");
            Instant start = Instant.now();
            while (Duration.between(start, Instant.now()).toMinutes() < MAX_TIME_WORK) {
                Socket socket;
                try {
                    socket = server.accept();
                } catch (SocketException e) {
                    System.out.println("MSocket server socket close by signal");
                    System.exit(-1);
                    break;
                }

                System.out.println("New tcpClient with adr: " + socket.getInetAddress());

                Sender sender = new Sender(socket);
                sendMsgs(sender);

                writeResults(sender);
            }
        }
        System.exit(-1);
    }

    private void sendMsgs(Sender sender) {
        File file = new File("input.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while (true) {
                String msg = br.readLine();
                if (msg == null) break;
                sender.send(msg);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void writeResults(Sender sender) throws IOException {
        Instant start2 = Instant.now();
        File file = new File("output.txt");
        file.deleteOnExit();
        file.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        while (Duration.between(start2, Instant.now()).toMinutes() < MAX_TIME_WORK) {
            try {
                String msg = sender.getIncome().poll(2, TimeUnit.MINUTES);
                System.out.println("receive: " + msg);

                bw.write(msg);
                bw.newLine();
                bw.flush();

            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    @Override
    public void run() {
        try {
            openSocket(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
