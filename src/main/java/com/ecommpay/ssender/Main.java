package com.ecommpay.ssender;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class Main {
    public static final int MAX_TIME_WORK = 2;

    public static void main(String[] args) throws IOException {
        int port = Integer.parseInt(args[0]);
        Thread t = new Thread(new MSocket("0.0.0.0", port));
        t.start();
        Instant start2 = Instant.now();
        while (Duration.between(start2, Instant.now()).toMinutes() < MAX_TIME_WORK) {

        }
        System.exit(-1);
    }

}
